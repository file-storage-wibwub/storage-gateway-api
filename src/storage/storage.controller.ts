import {
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  StreamableFile,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { createReadStream } from 'node:fs';
import * as fs from 'node:fs/promises';
import LocalFilesInterceptor from './local-storage/local-storage.interceptor';
import { StorageService } from './storage.service';

@Controller('v1/storage')
export class StorageController {
  constructor(
    private readonly storageService: StorageService,
    private readonly configService: ConfigService,
  ) { }

  @Post('/')
  @UseInterceptors(
    LocalFilesInterceptor({
      fieldName: 'file',
      path: '/',
    }),
  )
  uploadFile(@UploadedFile() file: Express.Multer.File) {
    return this.storageService.addFile(file);
  }

  @Get('/:objectID')
  async getFile(@Param('objectID') objectID: string) {
    const data = await this.storageService.getFile(objectID);
    const tempPath =
      this.configService.get('storage_path', './storages') + '/' + objectID;
    await fs.writeFile(tempPath, Buffer.from(data.buffer));
    const file = createReadStream(tempPath);
    return new StreamableFile(file, { type: data.contentType });
  }

  @Put('/:objectId')
  @UseInterceptors(
    LocalFilesInterceptor({
      fieldName: 'file',
      path: '/',
    }),
  )
  uploadUpdateFile(
    @UploadedFile() file: Express.Multer.File,
    @Param('objectId') objectId: string,
  ) {
    return this.storageService.updateFile(objectId, file);
  }

  @Delete('/:objectID')
  deleteFile(@Param('objectID') objectID: string) {
    return this.storageService.deleteFile(objectID);
  }
}
