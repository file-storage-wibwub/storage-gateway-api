import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ClientProxyFactory, Transport } from '@nestjs/microservices';
import { StorageController } from './storage.controller';
import { StorageService } from './storage.service';

@Module({
  imports: [HttpModule],
  providers: [
    StorageService,
    {
      provide: 'StorageService',
      useFactory: (configService: ConfigService) => {
        const host = configService.get('STORAGE_HOST', 'localhost');
        const port = configService.get('STORAGE_PORT', 3001);
        return ClientProxyFactory.create({
          transport: Transport.TCP,
          options: {
            host,
            port,
          },
        });
      },
      inject: [ConfigService],
    },
  ],
  controllers: [StorageController],
})
export class StorageModule { }
