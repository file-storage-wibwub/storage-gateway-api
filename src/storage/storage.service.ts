import {
  BadRequestException,
  Inject,
  Injectable,
  Logger,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ClientProxy } from '@nestjs/microservices';
import * as fs from 'node:fs/promises';
import { firstValueFrom } from 'rxjs';
import { v4 as uuidv4 } from 'uuid';
import { getFileResponse } from './dto/file.dto';

@Injectable()
export class StorageService {
  private logger: Logger;
  constructor(
    @Inject('StorageService') private readonly storageService: ClientProxy,
    private readonly configService: ConfigService,
  ) {
    const logger = new Logger('storage');
    this.logger = logger;
  }

  async addFile(file: Express.Multer.File) {
    if (!file) {
      throw new BadRequestException('file field is required');
    }
    this.logger.log('Create File to Core-API');
    const objectId = uuidv4();
    const buffer = await fs.readFile(file.path);
    const response = await firstValueFrom(
      this.storageService.send('upload-file', {
        buffer: buffer,
        mimeType: file.mimetype,
        objectId: objectId,
      }),
    );
    fs.rm(file.path);
    return response;
  }

  async getFile(objectId: string) {
    this.logger.log('Request File from Core-API');
    const data = await firstValueFrom<getFileResponse>(
      this.storageService.send('get-file', { objectId: objectId }),
    );

    if (data?.error || data?.isDelete) {
      throw new BadRequestException('Invalid Object Id');
    }

    if (data.status === 'waiting') {
      throw new BadRequestException('File Process is not compelete');
    }

    return data;
  }

  async updateFile(objectId: string, file: Express.Multer.File) {
    this.logger.log('Request Update File from Core-API');
    this.logger.debug(file);
    const buffer = await fs.readFile(file.path);
    const data = await firstValueFrom<getFileResponse>(
      this.storageService.send('update-file', {
        objectId: objectId,
        buffer: buffer,
        mimeType: file.mimetype,
      }),
    );
    fs.rm(file.path);
    return data;
  }

  async deleteFile(objectId: string) {
    this.logger.log('Request Delete File from Core-API');
    const data = await firstValueFrom<getFileResponse>(
      this.storageService.send('delete-file', { objectId: objectId }),
    );

    if (data?.error) {
      throw new BadRequestException('Invalid Object Id');
    }
    return data;
  }
}
