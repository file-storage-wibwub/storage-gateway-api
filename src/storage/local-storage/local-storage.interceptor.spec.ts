import { LocalStorageInterceptor } from './local-storage.interceptor';

describe('LocalStorageInterceptor', () => {
  it('should be defined', () => {
    expect(new LocalStorageInterceptor()).toBeDefined();
  });
});
