export type getFileResponse = {
  uuid: string;
  objectID: string;
  uploaderID: string;
  uploadedAt?: Date;
  status: string;
  contentType?: string;
  isDelete: boolean;
  deletedAt?: Date;
  createdAt: string;
  deleteBy: string;
  expiresDate?: Date;
  path?: string;
  buffer?: Buffer;
  error?: string;
};
