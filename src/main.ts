import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const port = process.env.port || 3000;
  const app = await NestFactory.create(AppModule);
  const logger = new Logger();
  await app.listen(port);
  logger.log('Start Application at ' + port);
}
bootstrap();
